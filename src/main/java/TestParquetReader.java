import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import parquet.example.data.Group;
import parquet.filter2.predicate.FilterPredicate;
import parquet.hadoop.example.ExampleInputFormat;

import java.io.IOException;

import static parquet.filter2.predicate.FilterApi.eq;
import static parquet.filter2.predicate.FilterApi.intColumn;

public class TestParquetReader extends Configured implements Tool {
    private static final Logger LOG = Logger.getLogger(TestParquetReader.class);

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new TestParquetReader(), args);
        System.exit(res);
    }

    Job GetJobConf(Configuration conf, String in_file, String out_dir) throws IOException {
        FilterPredicate pred = eq(intColumn("ip"), 1);
        ExampleInputFormat.setFilterPredicate(conf, pred);

        Job job = Job.getInstance(conf);
        job.setJarByClass(TestParquetReader.class);
        job.setJobName(TestParquetReader.class.getCanonicalName());

        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(ReadRequestMap.class);
        job.setNumReduceTasks(0);


        ExampleInputFormat.setTaskSideMetaData(job, false);
        job.setInputFormatClass(ExampleInputFormat.class);
        LOG.info(String.format("input filter: %s", ExampleInputFormat.getFilter(conf).toString()));

        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.setInputPaths(job, new Path(in_file));
        FileOutputFormat.setOutputPath(job, new Path(out_dir));

        return job;
    }

    public int run(String[] args) throws Exception {
        Job job = GetJobConf(getConf(), args[0], args[1]);
        return job.waitForCompletion(true) ? 0 : 1;
    }

    /*
     * Read (filtered) Parquet records, write them as text
     */
    public static class ReadRequestMap extends Mapper<LongWritable, Group, IntWritable, Text> {
        @Override
        public void map(LongWritable key, Group grp, Context context) throws IOException, InterruptedException {
            int ip = grp.getInteger("ip", 0);
            String payload = grp.getString("payload", 0);

            context.write(new IntWritable(ip), new Text(payload));
        }
    }
}