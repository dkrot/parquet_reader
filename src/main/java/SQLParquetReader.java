import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkJobInfo;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import parquet.filter2.predicate.FilterPredicate;
import parquet.hadoop.ParquetInputFormat;

import static parquet.filter2.predicate.FilterApi.eq;
import static parquet.filter2.predicate.FilterApi.intColumn;

public class SQLParquetReader {
    private final String in_file;

    SQLParquetReader(String in_file) {
        this.in_file = in_file;
    }

    JavaSparkContext getSparkContext() {
        SparkConf conf = new SparkConf().setAppName(SparkParquetReader.class.getCanonicalName());
        return new JavaSparkContext(conf);
    }

    void run() throws Exception {
        JavaSparkContext sc = getSparkContext();
        Configuration conf = sc.hadoopConfiguration();

        SQLContext sqlContext = new SQLContext(sc);

        DataFrame df = sqlContext.read().parquet(in_file);
        Column filter = df.col("src_ip_hash").equalTo(0)
                .and(df.col("src_ip").equalTo("0.0.0.0"));
        df.filter(filter).show();
        Thread.sleep(10000000);
    }

    static public void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.exit(64);
        }

        new SQLParquetReader(args[0]).run();
    }
}
