import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import parquet.example.data.Group;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import parquet.filter2.predicate.FilterPredicate;
import parquet.hadoop.example.ExampleInputFormat;

import java.io.IOException;

import static parquet.filter2.predicate.FilterApi.eq;
import static parquet.filter2.predicate.FilterApi.intColumn;


public class SparkParquetReader {
    private final String in_file;

    SparkParquetReader(String in_file) {
        this.in_file = in_file;
    }

    JavaSparkContext getSparkContext() {
        SparkConf conf = new SparkConf().setAppName(SparkParquetReader.class.getCanonicalName());
        return new JavaSparkContext(conf);
    }

    Job GetParquetJob(Configuration conf) throws IOException {
        Job job = Job.getInstance(conf);

        FilterPredicate pred = eq(intColumn("ip"), 1);

        ExampleInputFormat.setFilterPredicate(job.getConfiguration(), pred);
        ExampleInputFormat.setTaskSideMetaData(job, false);
        FileInputFormat.setInputPaths(job, new Path(in_file));
        return job;
    }

    void run() throws Exception {
        JavaSparkContext sc = getSparkContext();


        Job parquet_read_job = GetParquetJob(sc.hadoopConfiguration());

        long n = sc.newAPIHadoopRDD(parquet_read_job.getConfiguration(), ExampleInputFormat.class, Void.class, Group.class)
                .values()
                .count();

        System.out.printf("%d records matched\n", n);
    }

    static public void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.exit(64);
        }

        new SparkParquetReader(args[0]).run();
    }
}